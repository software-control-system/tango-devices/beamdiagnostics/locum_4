from conan import ConanFile

class LoCuM_4Recipe(ConanFile):
    name = "locum_4"
    executable = "ds_LoCuM_4"
    version = "3.2.0"
    package_type = "application"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Device"

    license = "GPL-3.0-or-later"    
    author = "Nicolas Leclercq"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/beamdiagnostics/locum_4.git"
    description = "LoCuM_4 device"
    topics = ("control-system", "tango", "device")

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = "CMakeLists.txt", "src/*"
    
    def requirements(self):
        self.requires("yat4tango/[>=1.0]@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")