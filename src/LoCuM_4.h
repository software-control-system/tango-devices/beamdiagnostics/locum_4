/*----- PROTECTED REGION ID(LoCuM_4.h) ENABLED START -----*/
//=============================================================================
//
// file :        LoCuM_4.h
//
// description : Include file for the LoCuM_4 class
//
// project :     LoCuM_4
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
// $Author:  $
//
// $Revision: 1.17 $
//
// $Log: not supported by cvs2svn $
// Revision 1.16  2012/10/12 10:21:59  xavela
// - config checked at init :
// -> range set to 1000. if not
// -> if Bias is not Minus and/or HV is not ON => device state is ALARM.
//
// Revision 1.15  2010/03/25 18:21:00  vince_soleil
// "Migration_Tango7"
//
// Revision 1.14  2008/10/10 09:40:42  buteau
// tentative de correction pour crash DSErver
//
// Revision 1.13  2007/11/21 11:29:17  sebleport
// no message
//
// Revision 1.12  2007/11/21 10:16:51  sebleport
// - code cleaning
// - remove XbpmLocum proxy property
// - "reset" command removed
// - dev_status() in comment
//
// Revision 1.11  2007/09/17 08:14:47  sebleport
// code cleaned
//
// Revision 1.10  2007/07/03 09:19:38  sebleport
// local / remote mode activated according to argin
// argin = true --> local mode
// agin = false --> remote mode
//
// Revision 1.9  2007/06/27 15:36:06  sebleport
// the gain attribute is now polled
//
// Revision 1.8  2006/10/30 15:36:46  sebleport
// changes of state management
//
// Revision 1.4  2006/09/26 15:15:20  sebleport
// new:
// - command setManualRange
// - severals attributes and command accessible by
//   expert view
// - inputCurrentRange attribute only READ
//
//
// copyleft :    European Synchrotron Radiation Facility
//               BP 220, Grenoble 38043
//               FRANCE
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#ifndef LoCuM_4_H
#define LoCuM_4_H

#include <tango.h>
#include <yat4tango/DeviceProxyHelper.h>
#include <TangoExceptionsHelper.h>
#include <Xstring.h>
#include <vector>

/**
 * @author	$Author: xavela $
 * @version	$Revision: 1.17 $
 */





/*----- PROTECTED REGION END -----*/	//	LoCuM_4.h

/**
 *  LoCuM_4 class description:
 *    controls the 4 channel Low-Current Monito called LoCuM_4. it consists of several components:
 *    - 4 current to voltage converters
 *    - an optically isolated amplifier
 *    - BIAS supply
 *    - Controller
 *    - an interface to host computer
 */

namespace LoCuM_4_ns
{
/*----- PROTECTED REGION ID(LoCuM_4::Additional Class Declarations) ENABLED START -----*/

//	Additional Class Declarations
//- (Serial) commands
static const std::string SERIAL_WRITE_CMD     = "DevSerWriteString";
static const std::string SERIAL_READ_CMD      = "DevSerReadRaw";
static const std::string SERIAL_READ_ONLY_CMD = "DevSerReadString";
//- (Ethernet) command
static const std::string ETH_WRITE_CMD        = "Write";
static const std::string ETH_READ_CMD         = "Read";
/*----- PROTECTED REGION END -----*/	//	LoCuM_4::Additional Class Declarations

class LoCuM_4 : public TANGO_BASE_CLASS
{

/*----- PROTECTED REGION ID(LoCuM_4::Data Members) ENABLED START -----*/

//	Add your own data members
public:



/*----- PROTECTED REGION END -----*/	//	LoCuM_4::Data Members

//	Device property data members
public:
	//	Bias:	Bias of the LoCuM, among : PLUS, MINUS, EXT, DEF
	string	bias;
	//	RangeList:	List of supported ranges
	vector<Tango::DevDouble>	rangeList;
	//	ComProtocol:	Communication protocol:
	//  - TCP
	//  - Serial
	string	comProtocol;
	//	ProxyName:	Device proxy:
	//  - ClientSocketServer device (TCP)
	//  - Serial device (Serial)
	string	proxyName;

//	Attribute data members
public:
	Tango::DevDouble	*attr_gain_read;

//	Constructors and destructors
public:
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device Name
	 */
	LoCuM_4(Tango::DeviceClass *cl,string &s);
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device Name
	 */
	LoCuM_4(Tango::DeviceClass *cl,const char *s);
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device name
	 *	@param d	Device description.
	 */
	LoCuM_4(Tango::DeviceClass *cl,const char *s,const char *d);
	/**
	 * The device object destructor.
	 */
	~LoCuM_4() {delete_device();};


//	Miscellaneous methods
public:
	/*
	 *	will be called at device destruction or at init command.
	 */
	void delete_device();
	/*
	 *	Initialize the device
	 */
	virtual void init_device();
	/*
	 *	Read the device properties from database
	 */
	void get_device_property();
	/*
	 *	Always executed method before execution command method.
	 */
	virtual void always_executed_hook();


//	Attribute methods
public:
	//--------------------------------------------------------
	/*
	 *	Method      : LoCuM_4::read_attr_hardware()
	 *	Description : Hardware acquisition for attributes.
	 */
	//--------------------------------------------------------
	virtual void read_attr_hardware(vector<long> &attr_list);
	//--------------------------------------------------------
	/*
	 *	Method      : LoCuM_4::write_attr_hardware()
	 *	Description : Hardware writing for attributes.
	 */
	//--------------------------------------------------------
	virtual void write_attr_hardware(vector<long> &attr_list);

/**
 *	Attribute gain related methods
 *	Description: to select input current  range.
 *
 *	Data type:	Tango::DevDouble
 *	Attr type:	Scalar
 */
	virtual void read_gain(Tango::Attribute &attr);
	virtual void write_gain(Tango::WAttribute &attr);
	virtual bool is_gain_allowed(Tango::AttReqType type);


	//--------------------------------------------------------
	/**
	 *	Method      : LoCuM_4::add_dynamic_attributes()
	 *	Description : Add dynamic attributes if any.
	 */
	//--------------------------------------------------------
	void add_dynamic_attributes();




//	Command related methods
public:
	/**
	 *	Command State related method
	 *	Description: This command gets the device state (stored in its <i>device_state</i> data member) and returns it to the caller.
	 *
	 *	@returns State Code
	 */
	virtual Tango::DevState dev_state();
	/**
	 *	Command Status related method
	 *	Description: This command gets the device status (stored in its <i>device_status</i> data member) and returns it to the caller.
	 *
	 *	@returns Status description
	 */
	virtual Tango::ConstDevString dev_status();
	/**
	 *	Command SetControlMode related method
	 *	Description: select the control mode
	 *
	 *	@param argin true: local, false:  remote
	 */
	virtual void set_control_mode(Tango::DevBoolean argin);
	virtual bool is_SetControlMode_allowed(const CORBA::Any &any);
	/**
	 *	Command SendLocum4Command related method
	 *	Description: send a Locum4 command. All commands are described in the Locum4 programmer's guide.
	 *
	 *	@param argin Locum4 command string
	 *	@returns response to the command
	 */
	virtual Tango::DevString send_locum4_command(Tango::DevString argin);
	virtual bool is_SendLocum4Command_allowed(const CORBA::Any &any);
	/**
	 *	Command RangeUp related method
	 *	Description: increase the range
	 *
	 */
	virtual void range_up();
	virtual bool is_RangeUp_allowed(const CORBA::Any &any);
	/**
	 *	Command RangeDown related method
	 *	Description: decrease the range
	 *
	 */
	virtual void range_down();
	virtual bool is_RangeDown_allowed(const CORBA::Any &any);
	/**
	 *	Command SetManualRange related method
	 *	Description: to set the manual mode
	 *
	 *	@param argin nothing
	 */
	virtual void set_manual_range(Tango::DevDouble argin);
	virtual bool is_SetManualRange_allowed(const CORBA::Any &any);


	//--------------------------------------------------------
	/**
	 *	Method      : LoCuM_4::add_dynamic_commands()
	 *	Description : Add dynamic commands if any.
	 */
	//--------------------------------------------------------
	void add_dynamic_commands();

/*----- PROTECTED REGION ID(LoCuM_4::Additional Method prototypes) ENABLED START -----*/

//	Additional Method prototypes
protected :

private:
	//data

  bool local;
  std::string _response,_cmd;
  std::string _dev_status;
  std::string _biasValue;
  bool _isHV_On;            //- High Voltage state
  bool _biasAlarm;          //- true if current bias differs from property value
  bool _biasDefined;        //- true if bias property is set
  bool _biasDefinedOk;      //- true if bias property is properly defined
  bool _biasSetAtInit;      //- true if bias value set at init
  bool _isEthernet;

  Tango::DevVarCharArray * _lf;

  //Tango::DeviceProxyHelper * _proxyDevice;
  yat::UniquePtr<yat4tango::DeviceProxyHelper> _proxyDevice;


  //methodes
  std::string write_read(std::string cmd_to_send,bool reading_expected);
  void create_proxy(void);
  double get_range(std::string config);
  std::string convert_range_double_to_string(double value);
  void get_config(void);
  void configure(void);
  void get_bias(std::string config);
  bool compare_bias();

  void check_range(Tango::DevDouble range); //- check if value is supported
  void set_gain(Tango::DevDouble);          //- method to apply a new gain (range) value.
  void update_writeGain(double);            //- update attr_gain_write attribute (to use the Tango memorized mecanism)

  //- Gain value memorisation in a property
template <class T>
void store_value_as_property(T value,std::string property_name);

void restore_saved_properties();


/*----- PROTECTED REGION END -----*/	//	LoCuM_4::Additional Method prototypes
};

/*----- PROTECTED REGION ID(LoCuM_4::Additional Classes Definitions) ENABLED START -----*/

//	Additional Classes Definitions

/*----- PROTECTED REGION END -----*/	//	LoCuM_4::Additional Classes Definitions

}	//	End of namespace

#endif   //	LoCuM_4_H
