# Microsoft Developer Studio Project File - Name="locum4" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) External Target" 0x0106

CFG=locum4 - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "locum4.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "locum4.mak" CFG="locum4 - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "locum4 - Win32 Release" (based on "Win32 (x86) External Target")
!MESSAGE "locum4 - Win32 Debug" (based on "Win32 (x86) External Target")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""

!IF  "$(CFG)" == "locum4 - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Cmd_Line "NMAKE /f locum4.mak"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "locum4.exe"
# PROP BASE Bsc_Name "locum4.bsc"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Cmd_Line "nmake /f Makefile.vc"
# PROP Rebuild_Opt "clean all"
# PROP Target_File "C:\DeviceServers\ds_LoCuM_4.exe"
# PROP Bsc_Name ""
# PROP Target_Dir ""

!ELSEIF  "$(CFG)" == "locum4 - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Cmd_Line "NMAKE /f locum4.mak"
# PROP BASE Rebuild_Opt "/a"
# PROP BASE Target_File "locum4.exe"
# PROP BASE Bsc_Name "locum4.bsc"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Cmd_Line "SET DEBUG=1 && nmake /f Makefile.vc"
# PROP Rebuild_Opt "clean all"
# PROP Target_File "C:\DeviceServers\ds_LoCuM_4.exe"
# PROP Bsc_Name ""
# PROP Target_Dir ""

!ENDIF 

# Begin Target

# Name "locum4 - Win32 Release"
# Name "locum4 - Win32 Debug"

!IF  "$(CFG)" == "locum4 - Win32 Release"

!ELSEIF  "$(CFG)" == "locum4 - Win32 Debug"

!ENDIF 

# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\ClassFactory.cpp
# End Source File
# Begin Source File

SOURCE=.\LoCuM_4.cpp
# End Source File
# Begin Source File

SOURCE=.\LoCuM_4Class.cpp
# End Source File
# Begin Source File

SOURCE=.\LoCuM_4StateMachine.cpp
# End Source File
# Begin Source File

SOURCE=.\main.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\LoCuM_4.h
# End Source File
# Begin Source File

SOURCE=.\LoCuM_4Class.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
